<%@ tag body-content="scriptless" %>
<%@ attribute name="addressBean" required="true" type="be.janolaerts.customtags.Opdracht4.AddressBean" %>

<p>Last name: ${addressBean.getLastName()}</p>
<p>First name: ${addressBean.getFirstName()}</p>
<p>Street: ${addressBean.getStreet()}</p>
<p>Bus: ${addressBean.getBus()}</p>
<p>Zip Code: ${addressBean.getZipCode()}</p>
<p>Community: ${addressBean.getCommunity()}</p>
<p>Country: ${addressBean.getCountry()}</p>
<p>Phone Number: ${addressBean.getPhoneNumber()}</p>
<p>Email: ${addressBean.getEmailAddress()}</p>
<jsp:doBody />