<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl" %>
<html>
<head>
    <title>Conversions</title>
</head>
<body>

    <p>15.5 km = ${mtl:kmToMiles(15.5)} miles</p>
    <p>15.5 degrees Celsius = ${mtl:celciusToFahrenheit(15.5)} degrees Fahrenheit</p>
    <p>15.5 meters = ${mtl:meterToCentimeter(15.5)} centimeters</p>
    <p>15.253695 rounded until 3 numbers = ${mtl:roundNumber(15.253695, 3)}</p>
</body>
</html>
