<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld" %>
<!DOCTYPE html>
<html>
<head>
    <title>Reverse Tag</title>
</head>
<body>

    <h1>Reverse Tag</h1>

    <form>
        <input type="text" name="text">
        <input type="submit" value="send">
    </form>
    <mtl:reverse text="${ param.text }"/>
</body>
</html>