<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld" %>
<!DOCTYPE html>
<html>
<head>
    <title>Calculate Tag</title>
</head>
<body>

    <form>
        <input type="number" name="num1">
        <input type="number" name="num2">
        <input type="submit" value="send">
    </form>
    <mtl:calculate number1="${param.num1}" number2="${param.num2}" />
</body>
</html>