<%@taglib tagdir="/WEB-INF/tags" prefix="tf" %>
<html>
<head>
    <title>Tag File</title>
</head>
<body>

    <tf:mytag size="20" color="blue">
        This text appears in blue and big letters
    </tf:mytag>
</body>
</html>