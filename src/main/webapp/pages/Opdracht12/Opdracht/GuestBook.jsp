<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Messages</title>
</head>
<body>

    <h1>Guest List: </h1>

<%--    Printing all the messages--%>
    <c:forEach items="${guestBookList}" var="item" varStatus="status" >
        <p>${item.getName()} ${item.getDate()}</p>
        <p>${item.getMessage()}</p>
        <p>---------------------------------------</p>
    </c:forEach>

<%--    Dynamically outputting logon and logout links--%>
    <c:choose>
        <c:when test="${request.isUserInRole('Guests')}">

            <form method="POST">

                Add a message:
                <textarea type='text' name='name' placeholder='name' ></textarea>
                <textarea type='text' name='message' placeholder='Write message: ' ></textarea>
                <input type='submit' value='send' />

                <a href='${pageContext.request.contextPath}/Logout' >Logout</a>
            </form>
        </c:when>
        <c:otherwise>

            <p>You are not a guest</p>
            <p>You are not allowed to add messages</p>
            <c:choose>
                <c:when test="${user != null}"><a href='${pageContext.request.contextPath}/Logout' >Logout</a></c:when>
                <c:otherwise><a href='${pageContext.request.contextPath}/Logon' >Logon</a></c:otherwise>
            </c:choose>

        </c:otherwise>

    </c:choose>

</body>
</html>