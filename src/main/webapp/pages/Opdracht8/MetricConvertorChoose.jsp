<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Metric Convertor Choose</title>
</head>
<body>

<form method="GET">
    <input type="number" step="0.01" name="meter" value="${param.meter}" /> meter
    <input type="submit" value="=">

    <c:choose>
        <c:when test="${param.containsValue('inch')}">
            <c:set var="factor" value="0.0254" scope="session" />
        </c:when>
        <c:when test="${param.containsValue('feet')}">
            <c:set var="factor" value="0.3048" scope="session" />
        </c:when>
        <c:when test="${param.containsValue('yard')}">
            <c:set var="factor" value="0.9144" scope="session" />
        </c:when>
        <c:when test="${param.containsValue('mile')}">
            <c:set var="factor" value="0.625" scope="session" />
        </c:when>
    </c:choose>

    <c:set var="result" value="${param.meter / factor}" />
    <c:out value="${result}" />

    <select name="conversions" onchange="this.form.submit()">
        <option value="inch" ${param.containsValue('inch') ? 'selected' : ""}>inch</option>
        <option value="feet" ${param.containsValue('feet') ? 'selected' : ""}>feet</option>
        <option value="yard" ${param.containsValue('yard') ? 'selected' : ""}>yard</option>
        <option value="mile" ${param.containsValue('mile') ? 'selected' : ""}>mile</option>
    </select>

</form>
</body>
</html>