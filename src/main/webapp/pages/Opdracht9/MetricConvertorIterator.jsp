<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Metric Convertor Iterator</title>
</head>
<body>

<form method="GET">

    <c:forEach items="${paramValues.conversions}" var="conversion" varStatus="status">
        <c:if test="${conversion == 'inch'}">
            <c:set var="factor" value="0.0254" />
        </c:if>
        <c:if test="${conversion == 'feet'}">
            <c:set var="factor" value="0.3084" />
        </c:if>
        <c:if test="${conversion == 'yard'}">
            <c:set var="factor" value="0.9144" />
        </c:if>
        <c:if test="${conversion == 'mile'}" >
            <c:set var="factor" value="0.625" />
        </c:if>
        ${param.meter} meter = ${param.meter / factor} ${conversion}<br/>
    </c:forEach>

    <input type="number" step="0.01" name="meter" value="${param.meter}" /> meter
    <input type="submit" value="=">

    <select name="conversions" multiple>
        <option value="inch" ${param.containsValue('inch') ? 'selected' : ""}>inch</option>
        <option value="feet" ${param.containsValue('feet') ? 'selected' : ""}>feet</option>
        <option value="yard" ${param.containsValue('yard') ? 'selected' : ""}>yard</option>
        <option value="mile" ${param.containsValue('mile') ? 'selected' : ""}>mile</option>
    </select>

</form>
</body>
</html>