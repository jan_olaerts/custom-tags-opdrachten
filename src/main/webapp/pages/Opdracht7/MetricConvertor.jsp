<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Metric Convertor</title>
</head>
<body>

<form>
    <input type="number" step="0.01" name="meter" /> meter
    <input type="submit" value="=">

    <c:set var="factor" value="0.0254" />
    <c:set var="inch" value="${param.meter / factor}" />
    <c:out value="${inch}" /> inch
</form>

</body>
</html>