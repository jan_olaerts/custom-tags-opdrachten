<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Metric Convertor Iterator</title>
</head>
<body>

<c:set var="locale" value="${pageContext.request.locale}" />
<fmt:setLocale value="${locale}" />

<fmt:bundle basename="be.janolaerts.customtags.Opdracht10.WelcomeBundle" prefix="language.">
    <c:if test="${locale.displayLanguage.equals('English')}">
        <fmt:message key="English" />
    </c:if>
    <c:if test="${locale.displayLanguage.equals('Dutch')}">
        <fmt:message key="Dutch" />
    </c:if>
    <c:if test="${locale.displayLanguage.equals('French')}">
        <fmt:message key="French" />
    </c:if>
    <c:if test="${locale.displayLanguage.equals('Spanish')}">
        <fmt:message key="Spanish" />
    </c:if>
</fmt:bundle>
<form method="GET">

    <c:forEach items="${paramValues.conversions}" var="conversion" varStatus="status">
        <c:if test="${conversion == 'inch'}">
            <c:set var="factor" value="0.0254" />
        </c:if>
        <c:if test="${conversion == 'feet'}">
            <c:set var="factor" value="0.3084" />
        </c:if>
        <c:if test="${conversion == 'yard'}">
            <c:set var="factor" value="0.9144" />
        </c:if>
        <c:if test="${conversion == 'mile'}" >
            <c:set var="factor" value="0.625" />
        </c:if>

        <c:catch var="e">
            <fmt:formatNumber type="number" value="${param.meter / factor}" pattern="#,##0.00" maxFractionDigits="2" var="result" />
        </c:catch>

        <c:choose>
            <c:when test="${e != null}">
                <c:out value="There is an exception: ${e}" /><br/>
                <c:out value="Exception message: ${e.message}" /><br/>
            </c:when>
            <c:otherwise>
                ${param.meter} meter = ${fn:trim(result)} ${conversion}<br/>
            </c:otherwise>
        </c:choose>

    </c:forEach>

    <input type="text" step="0.01" name="meter" value="${fn:trim(param.meter)}" /> meter
    <input type="submit" value="=">

    <select name="conversions" multiple>
        <option value="inch" ${param.containsValue('inch') ? 'selected' : ""}>inch</option>
        <option value="feet" ${param.containsValue('feet') ? 'selected' : ""}>feet</option>
        <option value="yard" ${param.containsValue('yard') ? 'selected' : ""}>yard</option>
        <option value="mile" ${param.containsValue('mile') ? 'selected' : ""}>mile</option>
    </select>

</form>
</body>
</html>