<%@ taglib prefix="mtl" uri="/WEB-INF/mytaglib.tld" %>
<!DOCTYPE html>
<html>
<head>
    <title>Uppercase Tag</title>
</head>
<body>

    <mtl:uppercase>
        This text should be in uppercase
    </mtl:uppercase>
</body>
</html>
