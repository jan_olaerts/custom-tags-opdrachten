<%@ taglib uri="/WEB-INF/mytaglib.tld" prefix="mtl" %>
<!DOCTYPE html>
<html>
<head>
    <title>Format Tag</title>
</head>
<body>

    <mtl:format style="text-decoration:underline; font-size:50px;">
        This text is formatted
    </mtl:format>
</body>
</html>