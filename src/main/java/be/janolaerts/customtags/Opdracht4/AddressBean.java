package be.janolaerts.customtags.Opdracht4;

import java.io.Serializable;

public class AddressBean implements Serializable {

    private String lastName;
    private String firstName;
    private String street;
    private String bus;
    private int zipCode;
    private String community;
    private String country;
    private String phoneNumber;
    private String emailAddress;

    public AddressBean() {
    }

    public AddressBean(String lastName, String firstName, String street, String bus, int zipCode, String community, String country, String phoneNumber, String emailAddress) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.street = street;
        this.bus = bus;
        this.zipCode = zipCode;
        this.community = community;
        this.country = country;
        this.phoneNumber = phoneNumber;
        this.emailAddress = emailAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        this.bus = bus;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "AddressBean{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", street='" + street + '\'' +
                ", bus=" + bus +
                ", zipCode=" + zipCode +
                ", community='" + community + '\'' +
                ", country='" + country + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}