package be.janolaerts.customtags.Opdracht4;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Address")
public class AddressServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        AddressBean addressBean = new AddressBean("Some", "Person", "Servlet Street", "50",
                123, "Servlet City", "Servlet Land", "Localhost:8080", "servlet@email.com");

        req.setAttribute("addressBean", addressBean);

        req.getRequestDispatcher("/pages/Opdracht4/AddressBean.jsp")
            .forward(req, resp);
    }
}