package be.janolaerts.customtags.Opdracht2;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class ReverseHandler extends SimpleTagSupport {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public void doTag() throws JspException, IOException {
        StringBuilder sb = new StringBuilder(text);
        JspWriter out = getJspContext().getOut();
        out.print(sb.reverse().toString());
    }
}