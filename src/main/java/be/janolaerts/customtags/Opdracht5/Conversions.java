package be.janolaerts.customtags.Opdracht5;

public class Conversions {

    public static String kmToMiles(double km) {
        return String.format("%.2f", km * 0.621);
    }

    public static String celciusToFahrenheit(double celcius) {
        return String.format("%.2f", celcius * (9d/5d) + 32);
    }

    public static String mToCm(double m) {
        return String.format("%.2f", m * 100);
    }

    public static String roundNumber(double num, int numOfDigits) {
        return String.format("%." + numOfDigits + "f", num);
    }
}