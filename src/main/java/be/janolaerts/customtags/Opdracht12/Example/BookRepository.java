package be.janolaerts.customtags.Opdracht12.Example;

import java.util.List;

public interface BookRepository {
    Book getBookByISBN(String isbn);
    List<Book> getAllBooks();
}