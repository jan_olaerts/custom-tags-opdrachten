package be.janolaerts.customtags.Opdracht12.Example;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/Books")
public class BookServlet extends HttpServlet {

    private BookRepository bookRepository = new BookRepositoryMemoryImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String isbn = req.getParameter("isbn");

        if(isbn == null) {
            List<Book> books = bookRepository.getAllBooks();
            req.setAttribute("books", books);

            req.getRequestDispatcher("/pages/Opdracht12/Example/BookList.jsp")
                    .forward(req, resp);

        } else {
            Book book = bookRepository.getBookByISBN(isbn);
            req.setAttribute("book", book);

            req.getRequestDispatcher("/pages/Opdracht12/Example/BookDetail.jsp")
                    .forward(req, resp);
        }
    }
}