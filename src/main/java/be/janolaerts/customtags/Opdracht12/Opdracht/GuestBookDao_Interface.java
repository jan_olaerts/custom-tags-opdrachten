package be.janolaerts.customtags.Opdracht12.Opdracht;

import java.sql.SQLException;
import java.util.List;

public interface GuestBookDao_Interface {

    List<GuestBook> getGuestBookItems() throws SQLException;
    void addGuestBookItem(GuestBook guestBook) throws SQLException;
}