package be.janolaerts.customtags.Opdracht12.Opdracht;

import javax.servlet.ServletException;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;

@WebServlet(name = "be.janolaerts.customtags.Opdracht12.Opdracht.GuestBookServlet",
            value = "/GuestBook",
            initParams = {
                @WebInitParam(name="driver", value="org.mariadb.jdbc.Driver"),
                @WebInitParam(name="url", value="jdbc:mariadb://javadev-training.be:3306/javadevt_Hever7"),
                @WebInitParam(name="user", value="javadevt_StudHe"),
                @WebInitParam(name="password", value="STUDENTvj2020")
            }
)
@ServletSecurity(value=@HttpConstraint(
    transportGuarantee=ServletSecurity.TransportGuarantee.CONFIDENTIAL),
    httpMethodConstraints=@HttpMethodConstraint(value="POST", rolesAllowed ="Guests"))
public class GuestBookServlet extends HttpServlet {

    GuestBookDao guestBookDao = new GuestBookDao();

    public void init() {

        guestBookDao.setDriver(getInitParameter("driver"));
        guestBookDao.setUrl(getInitParameter("url"));
        guestBookDao.setUser(getInitParameter("user"));
        guestBookDao.setPassword(getInitParameter("password"));
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            List<GuestBook> guestBookList = guestBookDao.getGuestBookItems();
            String user = request.getRemoteUser();

            request.setAttribute("guestBookList", guestBookList);
            request.setAttribute("user", user);
            request.setAttribute("request", request);

            request.getRequestDispatcher("/pages/Opdracht12/Opdracht/GuestBook.jsp")
                .forward(request, response);

        } catch (SQLException se) {
            throw new ServletException(se);
        }
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        try {
            String name = request.getParameter("name");
            String message = request.getParameter("message");
            Date now = Date.valueOf(LocalDate.now());
            guestBookDao.addGuestBookItem(new GuestBook(name, now, message));
        } catch (SQLException se) {
            throw new ServletException(se);
        }

        response.sendRedirect(request.getRequestURI());
    }
}