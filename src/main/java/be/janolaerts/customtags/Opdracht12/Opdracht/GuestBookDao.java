package be.janolaerts.customtags.Opdracht12.Opdracht;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GuestBookDao implements GuestBookDao_Interface {

    String driver;
    String url;
    String user;
    String password;

    private static final String SQL_GET_ALL_GUESTBOOKS = "SELECT * FROM Guestbooks;";
    private static final String SQL_ADD_GUESTBOOK =
            "INSERT INTO Guestbooks (name, date, message) VALUES (?, ?, ?);";

    public GuestBookDao() {
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName(driver);
        return DriverManager.getConnection(url, user, password);
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }
    public void setUrl(String url) {
        this.url = url;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public List<GuestBook> getGuestBookItems() throws SQLException {

        List<GuestBook> guestBooks = new ArrayList<>();

        try(Connection con = getConnection();
            Statement stmt = con.createStatement()) {
            try {
                ResultSet rs = stmt.executeQuery(SQL_GET_ALL_GUESTBOOKS);
                while (rs.next()) {
                    String name = rs.getString("name");
                    Date date = rs.getDate("date");
                    String message = rs.getString("message");

                    guestBooks.add(new GuestBook(name, date, message));
                }
            } catch(SQLException se) {
                System.out.println(se.getMessage());
                throw new SQLException(se);
            }

        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
            throw new SQLException(se);
        }

        return guestBooks;
    }

    @Override
    public void addGuestBookItem(GuestBook guestBook) throws SQLException {

        Timestamp timeStamp;
        try (Connection con = getConnection();
            PreparedStatement stmt = con.prepareStatement(SQL_ADD_GUESTBOOK)) {

            try {
                timeStamp = new Timestamp(guestBook.getDate().getTime());
                stmt.setString(1, guestBook.getName());
                stmt.setTimestamp(2, timeStamp);
                stmt.setString(3, guestBook.getMessage());

                stmt.executeUpdate();
            } catch (SQLException se) {
                System.out.println(se.getMessage());
                throw new SQLException(se);
            }

        } catch (SQLException | ClassNotFoundException se) {
            System.out.println(se.getMessage());
            throw new SQLException(se);
        }
    }
}