package be.janolaerts.customtags.Opdracht10;

import java.util.ListResourceBundle;

public class WelcomeBundle extends ListResourceBundle {

    static final Object[][] contents = {
        { "language.English", "Welcome" },
        { "language.Dutch", "Welkom" },
        { "language.French", "Bienvenue" },
        { "language.Spanish", "Buenos Dias" }
    };

    @Override
    protected Object[][] getContents() {
        return contents;
    }
}