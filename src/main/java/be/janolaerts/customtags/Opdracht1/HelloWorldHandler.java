package be.janolaerts.customtags.Opdracht1;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class HelloWorldHandler extends SimpleTagSupport {

    public void doTag() throws JspException, IOException {

        @SuppressWarnings("resource")
        JspWriter out = getJspContext().getOut();
        out.print("Hello World");
    }
}