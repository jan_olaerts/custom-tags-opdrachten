package be.janolaerts.customtags.Opdracht3;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;

public class UppercaseHandler extends SimpleTagSupport {

    @Override
    public void doTag() throws JspException, IOException {

        JspWriter out = getJspContext().getOut();
        StringWriter writer = new StringWriter();
        StringBuilder sb = new StringBuilder();

        getJspBody().invoke(writer);

        sb.append("<p>");
        sb.append(writer.getBuffer());
        sb.append("</p>");

        out.write(sb.toString().toUpperCase());
    }
}