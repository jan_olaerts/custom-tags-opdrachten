package be.janolaerts.customtags.Opdracht3;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class FormatHandler extends SimpleTagSupport implements DynamicAttributes {

    private Map<String, String> atts = new HashMap<>();

    @Override
    public void setDynamicAttribute(String uri, String name, Object value) throws JspException {

         if(uri == null) {
             // Only attributes from default namespace
             atts.put(name, (String) value);
         }
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();

        // Adding dynamic attributes
        out.print("<p ");
        out.print("style='color:red; font-weight:bold; ");
        for(String att : atts.keySet()) {
            if(att.equals("style")) {
                out.print(atts.get(att) + "' ");
            }
        }
        out.print(">");
        getJspBody().invoke(out);
        out.print("</p>");
    }
}