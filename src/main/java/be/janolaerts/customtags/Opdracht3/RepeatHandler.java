package be.janolaerts.customtags.Opdracht3;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.DynamicAttributes;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RepeatHandler extends SimpleTagSupport implements DynamicAttributes {

    private Map<String, String> atts = new HashMap<>();

    @Override
    public void setDynamicAttribute(String uri, String name, Object value) throws JspException {

        if(uri == null) {
            // Only attributes from default namespace
            atts.put(name, (String) value);
        }
    }

    @Override
    public void doTag() throws JspException, IOException {

        int count = 0;

        for(Map.Entry<String, String> entry : atts.entrySet()) {
            if(entry.getKey().equals("count")) {
                try {
                    count = Integer.parseInt(entry.getValue());
                } catch (NumberFormatException nfe) {
                    System.out.println("Value is not a number!");
                }
            }
        }

        JspWriter out = getJspContext().getOut();

        for(int i = 0; i < count; i++) {
            out.print("<p>");
            getJspBody().invoke(out);
            out.print("</p>");
        }
    }
}